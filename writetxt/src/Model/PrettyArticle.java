package Model;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by chabrol on 5/9/16.
 */
public class PrettyArticle {
    String qNumber;
    int numLanguages;
    LinkedHashMap<String,Integer> categories;

    public PrettyArticle(String qNumber, int numLanguages, List<Category> categories) {
        this.qNumber = qNumber;
        this.numLanguages = numLanguages;
        this.categories = new LinkedHashMap<>();
        for(Category c : categories) {
            this.categories.put(c.qNumber, c.getNumLanguages());
        }
    }
}
