package Model;

/**
 * Created by chabrol on 5/9/16.
 */

public class Category implements Comparable {
    public Article article;
    public String name;
    public String qNumber;                                    // qnumber of the category
    public int numLanguages;                                       //number of time it appears in every languages

    public Category(String qNumber, Article article) {              //constructor
        this.qNumber = qNumber;
        this.article = article;
        numLanguages = 1;
    }

    public Category(String qNumber,Article article,int numLanguages){
        this.qNumber = qNumber;
        this.article = article;
        this.numLanguages = numLanguages;
    }

    public void addOne(){                              //when we find the category in a new language
        numLanguages+=1;
    }

    public int getNumLanguages() {
        return numLanguages;
    }




    @Override
    public int compareTo(Object o) {
        Category cat = (Category) o;
        if (numLanguages < cat.getNumLanguages()) {return 1;}
        else if (numLanguages == cat.getNumLanguages()) {return 0;}
        else return -1;
    }
}