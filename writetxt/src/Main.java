
import Model.Article;
import Parse.GeneralParser;
import Parse.TqlParser;

import Parse.writeMap;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        long time_debut = System.currentTimeMillis();

        HashMap<String, Article> generalMap = new HashMap<>();
        int numLanguages = 0;

        File langRoot = new File("../lang");
        for (String language : langRoot.list()) {
            String filePath = "../lang/" + language + "/article_categories_" + language + ".tql";
            String mapPath = "../lang/" + language + "/articles_" + language + ".tsv";
            String dataPath = "../lang/" + language + "/categories_new_" + language + ".tsv";

            System.out.print("parsing" + "\t" + language + "\t");
            HashMap<String, ArrayList<String>> qNumberMap = TqlParser.readDbPedia(mapPath, dataPath, filePath);
            System.out.print("\t :" + "done" + "\t" + numLanguages+"\n" );

            GeneralParser.generalMatcher(qNumberMap, generalMap);

            numLanguages++;
        }

        System.out.print("writing map");

        writeMap.writeMaptxt(generalMap);

        System.out.print("Map created and written with \t" +numLanguages +"\t languages\n");

        System.out.print("The program took : ");
        System.out.println((System.currentTimeMillis()-time_debut)/60000);
        System.out.print(" minutes \n");



    }
}
