package Parse;

import Model.Article;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by chabrol on 4/20/16.
 */
public class GeneralParser {


    /*********************************************************************************
     * generalMatcher add the articles and categories of on language to the general map that collects the datas of every languages
     * @param languageMap    map linking article and category in one language
     * @param generalMap    map linking article and category and counts their frequencies in every languages
     */


    public static void generalMatcher(HashMap<String,ArrayList<String>> languageMap, HashMap<String, Article> generalMap){

        for (String articleqNumber: languageMap.keySet()){                                   // scroll through every article of the language map

            if (generalMap.containsKey(articleqNumber)){                                     //if this article already exists in the general map

                generalMap.get(articleqNumber).addOne();                                     //+1 its counter

                for(String categoryqNumber : languageMap.get(articleqNumber)){               // scroll through every category of this article
                    generalMap.get(articleqNumber).addCategory(categoryqNumber);
                }
            }

            else {
                Article article = new Article(articleqNumber);                                   //if the article does not exist in the general map

                // create an article with all its categories in the language map
                languageMap.get(articleqNumber).forEach(article::addCategory);

                generalMap.put( articleqNumber , article );                                      //and add it to the general map

            }
        }
    }






}