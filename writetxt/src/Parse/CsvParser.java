package Parse;

import javafx.util.Pair;

import java.io.*;
import java.util.HashMap;

/**
 * Created by dauvid on 2016-04-15.
 */
public class CsvParser {
    /****
     * create a map from a text file
     * @param filePath path to the text file, the file has the qnumber on the left and names on the right separated by a tab
     * @return    map with names as keys and qnumbers as value
     */
    public static HashMap<String,String> createMap(String filePath) {
        HashMap<String, String> map = new HashMap<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String mapLine;
            String[] mapLine_i;

            while (((mapLine = br.readLine()) != null)) {                     //reading the file line by line
                mapLine_i = mapLine.split("\t");                                     //split on tab
                map.put(mapLine_i[1], mapLine_i[0]);                                //switch the two parts and put them in a map
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        return map;
    }
}