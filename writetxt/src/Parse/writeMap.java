package Parse;

import Model.Article;
import Model.Category;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;


/**
 * Created by chabrol on 5/3/16.
 */
public class writeMap {


    public static void writeMaptxt(HashMap<String, Article> generalMap){


        try {
            BufferedWriter buff_new = new BufferedWriter(new FileWriter("generalMap.txt"));

            for (Article article : generalMap.values()){
                if (article.qNumber != null) {
                    buff_new.write(article.qNumber + "\t" + article.numLanguages + "\t");
                    for (Category category : article.categories.values()) {

                        buff_new.write(category.qNumber + "\t" + category.numLanguages + "\t");
                    }
                    buff_new.write("\n");
                    buff_new.flush();
                }
            }

            buff_new.close();



        }
        catch (IOException e) {
            System.out.print(e.getMessage());
        }

    }
}
