package Parse;


import Model.Article;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dauvid on 2016-04-15.
 */
public class TqlParser {
    /**
     * Helper method, used by parseText and parseFile
     * Extracts articles and categories from each line of a stream
     */
    public static HashMap<String, Article> parse(InputStream stream, HashMap<String, String> translationMap) throws IOException {
        HashMap<String, Article> results = new HashMap<>();
        BufferedReader bf = new BufferedReader(new InputStreamReader(stream));
        String line;
        while ((line = bf.readLine()) != null) {
            String[] temp = line.split("\t");
            String articleUrl = temp[0];
            String categoryUrl = temp[2];

            // Extract article/category names
            String article = extractFromUrl(articleUrl);
            String category = extractFromUrl(categoryUrl);

            //translate to Qnumbers
            String articleQNumber = translateToQNumber(article, translationMap);
            String categoryQNumber = translateToQNumber(category, translationMap);

            if (!results.containsKey(article)) {
                Article a = new Article(articleQNumber);
                a.addCategory(categoryQNumber);
                results.put(articleQNumber, a);
            } else {
                results.get(articleQNumber).addCategory(categoryQNumber);
            }
        }
        return results;
    }

    private static String extractFromUrl(String s) {
        Pattern p = Pattern.compile("\\/[^>\\/]+>");
        Matcher m = p.matcher(s);
        if (m.find()) {
            String temp = m.group(0);
            return temp.substring(1, temp.length() - 1);
        } else {
            throw new IllegalArgumentException("This shouldn't happen, wrong format on input file...");
        }
    }

    private static String translateToQNumber(String name, HashMap<String, String> translationMap) {
        if (!translationMap.containsKey(name)) {
            throw new IllegalArgumentException("No qnumber exists for " + name);
        }
        return translationMap.get(name);
    }


    /**
     * treat a file from dbpedia and create a map
     *
     * @param articleFilePath  path to the text file linking articles to their qnumbers
     * @param categoryFilePath path to the text file linking categories to their qnumbers
     * @param dbPediaFilePath  path to text file with every article linked to every of its categories in one language
     * @return a map with qnumbers of the articles as keys and qnumbers of the categories linked to it as values
     */
    public static HashMap<String, ArrayList<String>> readDbPedia(String articleFilePath, String categoryFilePath, String dbPediaFilePath) {
        Map<String, String> articleMap = CsvParser.createMap(articleFilePath);
        Map<String, String> categoryMap = CsvParser.createMap(categoryFilePath);

        HashMap<String, ArrayList<String>> results = new HashMap<>();
        HashMap<String, ArrayList<String>> finalMap = new HashMap<>();

        try {
            BufferedReader buff = new BufferedReader(new FileReader(dbPediaFilePath));
            String line;


            line = buff.readLine();                                                     //take out the first line

            while (line  != null) {

                treateLine(line, categoryMap, results);
                line = buff.readLine();                                                          //lines counter
            }
            buff.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

        for (String key : results.keySet()) {
            finalMap.put(articleMap.get(key), results.get(key));
        }

        return finalMap;

    }

    public static void treateLine(String line, Map<String, String> categoryMap, HashMap<String, ArrayList<String>> results) {
        String article;
        String category;
        Pattern pattern = Pattern.compile(":[^>\\/]+?>");

        String[] line_i = line.split("\\s+");                                      //split lines per blank

        Matcher matcher = pattern.matcher(line_i[2]);                             //select the article
        if (line_i.length > 3) {
            article = line_i[3].split("\\?")[0];
            article = article.substring(1);


            if (matcher.find()) {                                                     //select the category
                category = matcher.group(0);
                category = category.substring(1, category.length() - 1);

                if (categoryMap.get(category) == null) {
                    return;
                }
                category = categoryMap.get(category);


                if (results.containsKey(article)) {                                   //add category to the key value in results
                    results.get(article).add(category);
                } else {
                    ArrayList<String> temp = new ArrayList<>();                     //or add a new key if it doesn't already exist
                    temp.add(category);
                    results.put(article, temp);
                }
            }
        }
    }
}
