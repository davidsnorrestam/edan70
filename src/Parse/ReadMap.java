package Parse;

import Model.Article;
import Model.Category;

import java.io.*;
import java.util.HashMap;


/**
 * Created by chabrol on 5/9/16.
 */
public class ReadMap {

    public static HashMap<String, Article> ReadMaptxt(String datapath){
        HashMap<String,Article> generalMap = new HashMap<>();

        try {

            BufferedReader buff = new BufferedReader(new FileReader(datapath));


            String line;
            String qArticle;



            while (((line = buff.readLine()) != null)) {


                String[] line_i = line.split("\t");
                int nb_cat = (line_i.length-2)/2;

                qArticle = line_i[0];
                Article article= new Article(line_i[0]);
                article.numLanguages = Integer.parseInt(line_i[1]);

                for (int i=0; i < nb_cat ; i++){

                    Category category= new Category(line_i[2+2*i],article,Integer.parseInt(line_i[2+2*i+1]));
                    article.categories.put(line_i[2+2*i],category);
                }
                generalMap.put(qArticle,article);





            }

            buff.close();
        }
        catch (IOException e) {
            System.out.print(e.getMessage());
        }

    return generalMap;


    }
}
