package Parse;

import Model.Article;
import Model.Category;
import Model.Resources;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.HashMap;


/**
 * Created by chabrol on 4/27/16.
 */
public class ReverseParser {

    public static Article returnToNames(Article article) {


        article.name = Resources.qNumberToArticleMap.get(article.qNumber);

        String[] nullCategory = new String[article.categories.values().size()];
        int nNull =0;
        for (Category category : article.categories.values()) {
            if (Resources.qNumberToCategoryMap.get(category.qNumber) == null){
                nullCategory[nNull]= category.qNumber;
                nNull+=1;
            }
            else {
                category.name = (Resources.qNumberToCategoryMap.get(category.qNumber));

            }

        }
        if (nullCategory != null) {
            for (String nullQNumber : nullCategory)
                article.getCategories().remove(nullQNumber);
        }

        return article;




    }


        public static HashMap<String, String> createMapCategory (String filePath){
            HashMap<String, String> map = new HashMap<>();
            try {
                BufferedReader br = new BufferedReader(new FileReader(filePath));
                String mapLine;
                String[] mapLine_i;

                while (((mapLine = br.readLine()) != null)) {                     //reading the file line by line
                    mapLine_i = mapLine.split("\t");                                     //split on tab
                    map.put(mapLine_i[0], mapLine_i[1]);                                //switch the two parts and put them in a map
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
                System.exit(1);
            }
            return map;
        }

        public static HashMap<String,String> createMapArticle(String filePath){
            HashMap<String, String> map = new HashMap<>();
            try {
                BufferedReader br = new BufferedReader(new FileReader(filePath));
                String mapLine;
                String[] mapLine_i;


                while (((mapLine = br.readLine()) != null)) {                     //reading the file line by line
                    mapLine_i = mapLine.split("\t");//split on tab
                    map.put(mapLine_i[0], mapLine_i[1].substring(29));
                }



            } catch (IOException e) {
                System.out.println(e.getMessage());
                System.exit(1);
            }

            return map;

        }


}

