package WebServer;

import Interface.WebInterface;
import net.codestory.http.WebServer;
import net.codestory.http.templating.ModelAndView;

public class SunflowerServer {
    private WebServer webServer;
    private WebInterface webInterface;

    public static void main(String[] args) {
        SunflowerServer s = new SunflowerServer();
        s.start();
    }

    public void initiateWebInterface() {
        webInterface = new WebInterface();
    }

    public void start() {
        webServer = new WebServer().configure(routes -> routes
                .add(new SunflowerResource(webInterface))
                .get("/", ModelAndView.of("index"))).start();
    }

    public void stop() {
        webServer.stop();
    }
}