package WebServer.WebServer.JSONObjects;

/**
 *  Represents a Node to be displayed in the WebPage version of Sunflower
 */
public class NodeJSON {
    /**
     * Name of the Article/Category represented by the node
     */
    public String name;

    /**
     * String representing the group the node belongs to
     * One of the following:
     *  - Article
     *  - Category
     */
    public String group;

    /**
     * Ratio represents how "linked" a category is to an article and is calculated as follows:
     *
     *  1. If node has ONE edge in
     *
     *                              #Languages for which parent belongs to Category
     *  r1(parent) =  parent.ratio * ---------------------------------------------
     *                              #Languages parent occur in
     *
     *  2. If node is a nth order node with multiple edges in
     *
     *      r = Sum (i = 1..n) -> r1(parent[i])
     */
    public double ratio;

    public NodeJSON(String name, String group, double ratio) {
        this.name = name;
        this.group = group;
        this.ratio = ratio;
    }
}
