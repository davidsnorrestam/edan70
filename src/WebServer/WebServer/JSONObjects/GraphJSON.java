package WebServer.WebServer.JSONObjects;

import Model.Article;
import Model.Category;
import Model.Resources;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dauvid on 2016-05-02.
 */
public class GraphJSON {
    private final int width;
    private final int depth;
    public List<NodeJSON> nodes;
    public List<LinkJSON> links;

    public GraphJSON(int width, int depth) {
        this.width = width;
        this.depth = depth;
        nodes = new ArrayList<>();
        links = new ArrayList<>();
    }

    public static GraphJSON createFromArticle(Article article, int width, int depth) {
        GraphJSON graphJSON = new GraphJSON(width, depth);
        graphJSON.nodes.add(new NodeJSON(article.name, "Article", 1.0));

        // Loop through all categories and call add Node
        for(Category c : article.getMostImportant(width)) {
            graphJSON.addNode(c, 0, 1);
        }

        return graphJSON;
    }

    public void addNode(Category c, int parentIndex, int order) {
        if (c.name == null) {
            c.name = Resources.qNumberToCategoryMap.get(c.qNumber);

            if (c.name == null) {c.name = c.qNumber;}
        }
        if(indexOf(c.name) != -1) {
            // Update ratio
            int index = indexOf(c.name);
            double ratioToAdd = nodes.get(parentIndex).ratio * c.getRatio();
            nodes.get(index).ratio += ratioToAdd;
            if (nodes.get(index).ratio >1) nodes.get(index).ratio = 1;
            // Add link
            links.add(new LinkJSON(parentIndex, index));

        } else {
            int nodeIndex = nodes.size(); // Index of node to add is equal to the size of the list of nodes
            NodeJSON n = new NodeJSON(c.name, "Category, order " + order, c.getRatio() * nodes.get(parentIndex).ratio);
            nodes.add(n);
            links.add(new LinkJSON(parentIndex, nodeIndex));


            Article subCategory = Resources.categoryMap.get(c.qNumber);

            // Only recur if order has not reached max depth yet
            if (order < depth && subCategory != null && !subCategory.categories.isEmpty()) {
                for (Category parentCat :
                        subCategory.getMostImportant(width)) {
                    addNode(parentCat, nodeIndex, order + 1);
                }
            }
        }
    }

    public int indexOf(String nodeName) {
        for(int i = 0; i < nodes.size(); i++) {
            if(nodes.get(i).name.equals(nodeName) && !nodes.get(i).group.equals("Article")) {
                return i;
            }
        }
        return -1;
    }
}
