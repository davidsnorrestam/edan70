package WebServer.WebServer.JSONObjects;

/**
 * Represents a link between nodes on the Sunflower WebPage
 */
public class LinkJSON {
    public int source;
    public int target;
    public String type;
    public LinkJSON(int source, int target) {
        this.source = source;
        this.target = target;
        this.type ="suit";
    }
}
