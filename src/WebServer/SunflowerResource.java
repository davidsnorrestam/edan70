package WebServer;

import Interface.WebInterface;
import Model.Article;
import WebServer.WebServer.JSONObjects.GraphJSON;
import com.google.gson.Gson;
import net.codestory.http.annotations.Get;
import net.codestory.http.annotations.Prefix;

/**
 * Created by dauvid on 2016-05-02.
 */
@Prefix("/sunflower")
public class SunflowerResource {
    WebInterface webInterface;

    public SunflowerResource(WebInterface webInterface) {
    this.webInterface = webInterface;
    }

    @Get("/:id/:width/:depth")
    public String searchFor(String id, int width, int depth) {
        Article article = webInterface.searchFor(id, width);
        GraphJSON graphJSON = GraphJSON.createFromArticle(article, width, depth);

        Gson gson = new Gson();
        return gson.toJson(graphJSON);
    }
}
