package Test;

import Control.Sunflower;
import ResultModel.SunflowerResult;
import ResultModel.SunflowerResultNode;

import java.util.Scanner;

/**
 * Created by dauvid on 2016-05-11.
 */
public class TestInterface {
    private static Sunflower sunflower;
    private static int width;
    private static int depth;


    public static void main(String[] args) {
        width = 3;
        depth = 2;
        sunflower = new Sunflower();
        while(true) {
            read();
        }
    }

    private static void read() {
        Scanner s = new Scanner(System.in);

        System.out.println("Enter command:  ");
        System.out.println("\t 1. SetLanguage");
        System.out.println("\t 2. SearchFor");
        System.out.println("Enter command:  ");
        String command = s.nextLine();
        switch(command)  {
            case "1":
                setLanguage();
                break;
            case "2":
                searchFor();
                break;

        }
    }

    public static void setLanguage() {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter language sv/en/pl...");
        try {
            String lang = s.nextLine();
            sunflower.setLanguage(lang);
        } catch (Exception e) {
            System.out.println("Failed to load language files." +  e.getMessage());
        }
    }
    public static void searchFor() {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter concept");
            String concept = s.nextLine();
        try {
            SunflowerResult result = sunflower.searchFor(concept, width, depth);
        }
         catch (Exception e) {
             System.out.println("Something went wwrong..." +  e.getMessage());
         }
    }
}
