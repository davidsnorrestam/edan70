package ResultModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A node in the graph that represents the relation between an article and categories.
 */
public class SunflowerResultNode {
    private String qNumber;
    private double ratio;
    private SunflowerResultNode parent;
    private List<SunflowerResultNode> children;

    public SunflowerResultNode(String qNumber, double ratio) {
        this.qNumber = qNumber;
        this.ratio = ratio;
        children = new ArrayList<>();
    }

    public void addChild(SunflowerResultNode s) {
        s.parent = this;
        children.add(s);
    }

    public void addRatio(double ratio) {
        this.ratio += ratio;
    }

    public double getRatio() {
        return ratio;
    }

    public SunflowerResultNode getParent() {
        return parent;
    }

    public String getqNumber() {
        return qNumber;
    }

    public List<SunflowerResultNode> getChildren () {
        return children;
    }
}