package ResultModel;

import Model.Article;
import Model.Category;
import Model.Resources;

import java.util.HashMap;

/**
 * Created by dauvid on 2016-05-11.
 */
public class SunflowerResultList {
    String qNumber;
    HashMap<String, Double> categories;

    public SunflowerResultList() {
        categories = new HashMap<>();
    }
    public static SunflowerResultList createFromArticle(Article article, int width, int depth) {
        SunflowerResultList sfr = new SunflowerResultList();
        sfr.qNumber = article.qNumber;

        for(Category c : article.getMostImportant(width)) {
            sfr.categories.put(c.qNumber, c.getRatio());
            if (depth > 1) {
                addSubCategories(sfr, Resources.categoryMap.get(c.qNumber), width, depth - 1);
            }
        }
        return sfr;
    }
    private static void addSubCategories(SunflowerResultList sfr, Article subCategory, int width, int depth) {
        double parentRatio = sfr.categories.get(subCategory.qNumber);
        for(Category c : subCategory.getMostImportant(width)) {
            if(!sfr.categories.containsKey(c.qNumber)) {
                sfr.categories.put(c.qNumber, c.getRatio() * parentRatio);
            }
            else {
                sfr.categories.put(c.qNumber, sfr.categories.get(c.qNumber) + c.getRatio() * parentRatio);
            }

            if (depth > 1) {
                addSubCategories(sfr, Resources.categoryMap.get(c.qNumber), width, depth - 1);
            }
        }
    }
}
