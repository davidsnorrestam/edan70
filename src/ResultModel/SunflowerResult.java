package ResultModel;

/**
 * Created by dauvid on 2016-05-11.
 */
public class SunflowerResult {
    private SunflowerResultNode graph;
    private SunflowerResultList list;

    public SunflowerResult(SunflowerResultNode graph, SunflowerResultList list){
        this.graph = graph;
        this.list = list;
    }


    public SunflowerResultNode getGraph() {
        return graph;
    }

    public SunflowerResultList getList() {
        return list;
    }
}
