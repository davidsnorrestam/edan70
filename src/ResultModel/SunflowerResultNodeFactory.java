package ResultModel;

import Model.Article;
import Model.Category;
import Model.Resources;

import java.util.HashMap;

/**
 * Created by dauvid on 2016-05-11.
 */
public class SunflowerResultNodeFactory {
    private static HashMap<String, SunflowerResultNode> nodes;

    public static SunflowerResultNode createTreeFromArticle(Article article, int width, int depth) {
        SunflowerResultNode root = new SunflowerResultNode(article.qNumber, 1.0);
        nodes = new HashMap<>();
        buildSubTree(root, article, width, depth);
        return root;
    }

    /**
     * WARNING Changes the structure of root
     * @param root
     * @param article
     * @param width
     * @param depth
     */
    private static void buildSubTree(SunflowerResultNode root, Article article, int width, int depth) {
        for(Category c : article.getMostImportant(width)) {
            SunflowerResultNode child;

            if (!nodes.containsKey(c.qNumber)) {
                child = new SunflowerResultNode(c.qNumber, c.getRatio() * root.getRatio());
                nodes.put(c.qNumber, child);
            } else {
                child = nodes.get(c.qNumber);
                child.addRatio(c.getRatio() * root.getRatio());
            }
            root.addChild(child);

            // Call recursively if depth > 1
            if (depth > 1) {
                Article subCategory = Resources.categoryMap.get(c.qNumber);
                buildSubTree(child, subCategory, width, depth - 1);
            }
        }
    }
}
