package Control;

import Interface.Founder;
import Model.Article;
import Model.Resources;
import ResultModel.SunflowerResult;
import ResultModel.SunflowerResultList;
import ResultModel.SunflowerResultNode;
import ResultModel.SunflowerResultNodeFactory;
import WebServer.SunflowerServer;

public class Sunflower implements ISunflower{
    public static void main(String[] args) {
        Sunflower s = new Sunflower();

        s.setLanguage(args[0]);
        s.startServer();
    }

    public void setLanguage(String language) {
        Resources.initiateResources(language);
    }

    public void startServer() {
        SunflowerServer server = new SunflowerServer();
        server.initiateWebInterface();
        server.start();
    }

    @Override
    public SunflowerResult searchFor(String concept, int width, int depth) {
        Article article = Founder.findCategories(concept, Resources.generalMap, width);
        SunflowerResultNode graph =  SunflowerResultNodeFactory.createTreeFromArticle(article, width, depth);
        SunflowerResultList list = SunflowerResultList.createFromArticle(article, width, depth);

        return new SunflowerResult(graph, list);

    }
}