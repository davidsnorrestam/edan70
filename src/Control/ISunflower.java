package Control;

import ResultModel.SunflowerResult;
import ResultModel.SunflowerResultNode;

/**
 * Created by dauvid on 2016-05-11.
 */
public interface ISunflower {
    SunflowerResult searchFor(String concept, int width, int depth);
    void setLanguage(String language);
}
