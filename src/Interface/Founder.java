package Interface;

import Model.Article;
import Model.Category;
import Model.Resources;
import Parse.CsvParser;
import Parse.ReadMap;
import Parse.ReverseParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static Model.Resources.categoryMap;

/**
 * Created by chabrol on 4/28/16.
 */
public class Founder {
    public static Article findCategories(String articleName, HashMap<String, Article> generalMap,int width){


        String qnumberA = Resources.articleToQNumberMap.get(articleName);
        //String qnumberA=articleName;



        Article result = ReverseParser.returnToNames(generalMap.get(qnumberA));
        //Article result =generalMap.get(qnumberA);

        result.name = articleName;

        return result;
    }






    public static HashMap<String,String> createMapArticle(String filePath){
        HashMap<String, String> map = new HashMap<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String mapLine;
            String[] mapLine_i;



            while (((mapLine = br.readLine()) != null)) {                     //reading the file line by line
                mapLine_i = mapLine.split("\t");//split on tab

                map.put( mapLine_i[1].substring(29),mapLine_i[0]);


            }



        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

        return map;

    }
}
