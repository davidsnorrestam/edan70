package Interface;

import Model.Article;
import Model.Resources;

import java.util.HashMap;

/**
 * Created by chabrol on 5/10/16.
 */
public class WebInterface {
    public Article searchFor(String s, int width) {
        return Founder.findCategories(s, Resources.generalMap, width);
    }
}