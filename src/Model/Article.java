package Model;



import com.google.gson.Gson;



import java.util.*;



public class Article {
    public String qNumber;                           //qNumber of the article
    public int numLanguages;                         //number of languages it appears in

    public String name;
    public HashMap<String, Category> categories;

    public Article(String qNumber) {                  //constructor
        this.qNumber = qNumber;
        numLanguages = 1;

        categories = new HashMap<String, Category>();
    }



    public HashMap<String,Category> getCategories(){return categories;}

    public void addOne(){                            //when we find the article in a new language
        numLanguages+=1;
    }


    public void addCategory(String categoryQNumber) {
        if(!categories.containsKey(categoryQNumber)) {
            categories.put(categoryQNumber, new Category(categoryQNumber,this));
        } else {
            categories.get(categoryQNumber).addOne();
        }
    }

    /**
     * Get the most frequently ocurring categories for this article.
     * @param k number of categories to return
     * @return A list of the k most frequent categories*/




    public List<Category> getMostImportant(int k) {

        ArrayList<Category> list=  new ArrayList(categories.values());

        Collections.sort(list);

        if (list.size() > k) {
            return list.subList(0, k);
        }

        return list;
    }


    public String toString(int width) {

        List<Category> sortedCategories = getMostImportant(width);
        PrettyArticle pa = new PrettyArticle(name, numLanguages, sortedCategories);

        Gson gson = new Gson();
        return gson.toJson(pa);
    }


}
