package Model;

import Interface.Founder;
import Parse.ReadMap;
import Parse.ReverseParser;

import java.util.HashMap;

/**
 * Created by chabrol on 5/10/16.
 */
public class Resources {
    public static HashMap<String, Article> generalMap;
    public static HashMap<String, Article> categoryMap;
    public static HashMap<String, String> qNumberToArticleMap;
    public static HashMap<String, String> qNumberToCategoryMap;
    public static HashMap<String, String> articleToQNumberMap;


    public static void initiateResources(String lang) {
        generalMap = ReadMap.ReadMaptxt("GeneralMap.txt");
        categoryMap = ReadMap.ReadMaptxt("CategoryMap.txt");
        qNumberToArticleMap = ReverseParser.createMapArticle("lang/"+lang+"/articles_"+lang+".tsv");
        qNumberToCategoryMap = ReverseParser.createMapCategory("lang/"+lang+"/categories_new_"+lang+".tsv");
        articleToQNumberMap = Founder.createMapArticle("lang/"+lang+"/articles_"+lang+".tsv");
    }
}
