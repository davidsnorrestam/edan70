package Model;

import java.util.ArrayList;
import java.util.HashMap;
import Reader.ReadMap;

/**
 * Created by chabrol on 5/17/16.
 */
public class Resources {

    public static HashMap<String, ArrayList<Category>> generalMap;


    public static void initiateResources() {
        generalMap = ReadMap.ReadMapstxt("../GeneralMap.txt","../CategoryMap.txt");
    }

}
