package Model;

/**
 * Created by chabrol on 5/17/16.
 */
public class Category implements Comparable{
    private String qNumber;

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    private double ratio;

    public double getRatio() {
        return ratio;
    }
    public String getqNumber(){
        return qNumber;
    }




    public Category(String qNumberC,int numLanguagesC, int numLanguagesA){
        this.qNumber=qNumberC;
        this.ratio= ((double)numLanguagesC / numLanguagesA);
    }

    public Category(String qNumberC,double ratio){
        this.qNumber=qNumberC;
        this.ratio= ratio;
    }

    @Override
    public int compareTo(Object o) {
        Category cat = (Category) o;
        if (ratio < cat.getRatio()) {
            return 1;
        } else if (ratio == cat.getRatio()) {
            return 0;
        } else return -1;
    }
}
