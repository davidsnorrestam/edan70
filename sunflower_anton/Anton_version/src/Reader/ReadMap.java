package Reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import Model.Category;

/**
 * Created by chabrol on 5/17/16.
 */
public class ReadMap {

    public static HashMap<String, ArrayList<Category> > ReadMapstxt(String datapatharticle,String datapathcategory){
        HashMap<String,ArrayList<Category>> generalMap = new HashMap<>();

        try {

            BufferedReader buff = new BufferedReader(new FileReader(datapatharticle));


            String line;
            String qArticle;
            int numLanguages;



            while (((line = buff.readLine()) != null)) {


                String[] line_i = line.split("\t");
                int nb_cat = (line_i.length-2)/2;

                qArticle = line_i[0];
                numLanguages = Integer.parseInt(line_i[1]);

                ArrayList<Category> categories= new ArrayList<>();

                for (int i=0; i < nb_cat ; i++){

                    Category category= new Category(line_i[2+2*i],Integer.parseInt(line_i[2+2*i+1]),numLanguages);
                    categories.add(category);
                }
                Collections.sort(categories);

                generalMap.put(qArticle,categories);

            }

            buff.close();
        }
        catch (IOException e) {
            System.out.print(e.getMessage());
        }

        try {

            BufferedReader buff = new BufferedReader(new FileReader(datapathcategory));


            String line;
            String qArticle;
            int numLanguages;



            while (((line = buff.readLine()) != null)) {


                String[] line_i = line.split("\t");
                int nb_cat = (line_i.length-2)/2;

                qArticle = line_i[0];
                numLanguages = Integer.parseInt(line_i[1]);

                ArrayList<Category> categories= new ArrayList<>();

                for (int i=0; i < nb_cat ; i++){

                    Category category= new Category(line_i[2+2*i],Integer.parseInt(line_i[2+2*i+1]),numLanguages);
                    categories.add(category);
                }
                Collections.sort(categories);

                generalMap.put(qArticle,categories);

            }

            buff.close();
        }
        catch (IOException e) {
            System.out.print(e.getMessage());
        }

        return generalMap;


    }
}