package Founder;

import Model.Category;
import Model.Resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by chabrol on 5/17/16.
 */
public class LookFor {


    public static ArrayList<Category> findCategories(String qNumberA){

        return Resources.generalMap.get(qNumberA);
    }

    public static List<Category> findNBestCategories(String qNumberA, int width){
        ArrayList<Category> categories = Resources.generalMap.get(qNumberA);

        if (categories.size() > width) {
            return categories.subList(0, width);
        }
        return categories;
    }

    public static List<Category> findCategoryOver(String qNumberA, double ratiolimit){
        ArrayList<Category> categories= Resources.generalMap.get(qNumberA);
        int num=0;

        while (categories.get(num).getRatio() > ratiolimit ){
            num++;
        }

        return categories.subList(0,num);

    }

    public static List<Category> findCategorydepth(String qNumberA, int width, int depth){
        List<Category> categories=findNBestCategories(qNumberA,width);

       List<Category> categorieslastdepth=new ArrayList<>();
        for (Category category:categories){
            categorieslastdepth.add(category);
        }

        addnextdepth(categories,categorieslastdepth,width,depth);

        return categories;
    }


    public static void addnextdepth(List<Category> categories, List<Category> categorieslastdepth, int width,int depth){
        ArrayList<Category> nextdepth= new ArrayList<>();

        if (depth ==1) {
            Collections.sort(categories);
            return;
        }

        for (Category category : categorieslastdepth){
            List<Category> depth2=findNBestCategories(category.getqNumber(),width);
            for (Category categorydepth2 : depth2){
                int index=contains(categorydepth2.getqNumber(),categories);
                if (index > -1){
                    categories.get(index).setRatio(categories.get(index).getRatio()+categorydepth2.getRatio()*category.getRatio());

                }
                else {
                    Category newCategory= new Category(categorydepth2.getqNumber(),categorydepth2.getRatio()*category.getRatio());
                    categories.add(newCategory);
                    nextdepth.add(newCategory);
                }
            }
        }
        addnextdepth(categories,nextdepth,width,depth-1);
    }


    public static int contains(String qNumber,List<Category> categories){

        for (int i=0; i < categories.size() ; i++){
            if ( qNumber.equals(categories.get(i).getqNumber()) ) return i;
        }
        return -1;
    }

}
