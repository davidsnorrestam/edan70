// Action listeners
$(document).ready(function(){
$('#textfield').on('keypress', function(event) {
    if(event.which === 13) {

        updateData($("#textfield").val(), 1, 1);
    }
});
});




// Graph code
var width = 960,
    height = 500;

var color = d3.scale.category20();

var force = d3.layout.force()
    .charge(-120)
    .linkDistance(30)
    .size([width, height]);

var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

d3.json("miserables.json", function(error, graph) {
  if (error) throw error;

  force
      .nodes(graph.nodes)
      .links(graph.links)
      .start();

  var link = svg.selectAll(".link")
      .data(graph.links)
    .enter().append("line")
      .attr("class", "link")
      .style("stroke-width", function(d) { return Math.sqrt(d.value); });

  var node = svg.selectAll(".node")
      .data(graph.nodes)
    .enter().append("circle")
      .attr("class", "node")
      .attr("r", 5)
      .style("fill", function(d) { return color(d.group); })
      .call(force.drag);

  node.append("title")
      .text(function(d) { return d.name; });

  force.on("tick", function() {
    link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node.attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; });
  });
});
function updateData(searchString, widthNew, depthNew)
{
	width = widthNew;
	depth = depthNew;
	concept = searchString;

	d3.select("body").selectAll("svg").remove();

	//var url = "concept/graph/"+encodeURIComponent(searchString)+"?width="+width+"&depth="+depth+"&noPruning="+noPruning+"&fullLabels="+fullLabels;
    var url = "/sunflower/" + searchString + "/" + width + "/" + depth;

	d3.json(url, function(graph) {
	  console.log(graph);

        article = {'name': graph.name, 'importance': 1.0};

	    links = graph.links;
	    nodes = graph.nodes;


        links.forEach(function(link) {
	        link.source = nodes[link.source];
	        link.target = nodes[link.target];});

	    var w = 1000,
	        h = 600;

	    nodes[0].fixed = true;
	    nodes[0].x = w / 2;
	    nodes[0].y = h / 2;

	    var force = d3.layout.force()
	        .nodes(nodes)
	        .links(links)
	        .size([w, h])
	        .linkDistance(100)
	        .charge(-300)
	        .gravity(0.05)
	        .on("tick", tick)
	        .start();

	    var svg = d3.select("body").append("svg:svg")
	        .attr("width", w)
	        .attr("height", h);

	    // Per-type markers, as they don't inherit styles.
	    svg.append("svg:defs").selectAll("marker")
	        .data(["suit", "licensing", "resolved"])
	      .enter().append("svg:marker")
	        .attr("id", String)
	        .attr("viewBox", "0 -5 10 10")
	        .attr("refX", 15)
	        .attr("refY", -1.5)
	        .attr("markerWidth", 6)
	        .attr("markerHeight", 6)
	        .attr("orient", "auto")
	      .append("svg:path")
	        .attr("d", "M0,-5L10,0L0,5");

	    var path = svg.append("svg:g").selectAll("path")
	        .data(force.links())
	      .enter().append("svg:path")
	        .attr("class", function(d) { return "link " + d.type; })
	        .attr("marker-end", function(d) { return "url(#" + d.type + ")"; })
	        .style("stroke-width", 1);

	    var circle = svg.append("svg:g").selectAll("circle")
	        .data(force.nodes())
	      .enter().append("svg:circle")
	        .attr("r", function(d) {return 6*d.ratio})
	        .call(force.drag);
//
	    var text = svg.append("svg:g").selectAll("g")
	        .data(force.nodes())
	      .enter().append("svg:g");
//
//	    // A copy of the text with a thick white stroke for legibility.
	    text.append("svg:text")
	        .attr("x", 8)
	        .attr("y", ".31em")
	        .attr("class", "shadow")
	        .text(function(d) { return d.name; })
	        .style("font-size", function(d) { return d.group == "intermediate" ?  "10px" : "15px"; });
//
	    text.append("svg:text")
	        .attr("x", 8)
	        .attr("y", ".31em")
	        .attr("class", "front")
	        .text(function(d) { return d.name; })
	        .style("font-size", function(d) { return d.group == "intermediate" ?  "10px" : "15px"; })
	        .style("fill", function(d) { if( d.group == "Article" ) return "#D26534";
	                                     if( d.group == "pathOfSubcategories" ) return "#38B5A9";
	                                     if( d.group == "directCategory" ) return "#E1E316";
	                                     if( d.group == "linkType" ) return "#78C14C";
	                                     if( d.group == "intermediate" ) return "#666";
	                                     return "#0D5F8B"; });



	  /*
	  #0D5F8B
	  #38B5A9
	  #E1E316
	  #78C14C
	  #D26534
	  */

	    // Use elliptical arc path segments to doubly-encode directionality.
	    function tick() {
	      path.attr("d", function(d) {
	        var dx = d.target.x - d.source.x,
	            dy = d.target.y - d.source.y,
	            dr = Math.sqrt(dx * dx + dy * dy);
	        return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
	      });
////
	      circle.attr("transform", function(d) {
	        return "translate(" + d.x + "," + d.y + ")";
	      });
//
	      text.attr("transform", function(d) {
	        return "translate(" + d.x + "," + d.y + ")";
	      });
	    }
	});
}
function updateDataWidth(searchString, widthNew)
{
	updateData(searchString, widthNew, depth);
}

function updateDataDepth(searchString, depthNew)
{
	updateData(searchString, width, depthNew);
}
