import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        String lang = "yo";

        String skos = "../skos_categories_"+lang+".tql";
        String skos_new = "../skos_categories_new_"+lang+".tql";
        String category = "../categories_"+lang+".tsv";
        String category_new = "../categories_new_"+lang+".tsv";


        try {

            BufferedReader buffc = new BufferedReader(new FileReader(category));
            BufferedReader buffs = new BufferedReader(new FileReader(skos));

            BufferedWriter buffc_new = new BufferedWriter(new FileWriter(category_new));
            BufferedWriter buffs_new = new BufferedWriter(new FileWriter(skos_new));

            String lines;
            String linec;



            Pattern pattern02 = Pattern.compile(":[^>\\/]+");
            Pattern pattern = Pattern.compile("broader");

            String subcategorie;
            String categorie;



            while ((lines = buffs.readLine()) != null) {

                Matcher matcher=pattern.matcher(lines);
                if (matcher.find()) {
                    String[] line_i = lines.split(">");
                    Matcher matcher0 = pattern02.matcher(line_i[0]);
                    Matcher matcher2 = pattern02.matcher(line_i[2]);


                    if ((matcher0.find()) & (matcher2.find()) ) {

                        subcategorie=matcher0.group(0);
                        subcategorie=subcategorie.substring(1);


                        categorie=matcher2.group(0);
                        categorie=categorie.substring(1);

                        buffs_new.write(subcategorie + "\t" + categorie + "\n");
                        buffs_new.flush();


                    }


                }

            }
            buffs.close();
            buffs_new.close();

            while ((linec = buffc.readLine()) != null) {

                String[] line_i=linec.split("\t");

                Matcher matcherc=pattern02.matcher(line_i[1]);

                if (matcherc.find() ) {

                        categorie=matcherc.group(0);
                        categorie=categorie.substring(1);



                        buffc_new.write(line_i[0]+ "\t" + categorie + "\n");
                        buffc_new.flush();





                }

            }
            buffc_new.close();
            buffc.close();

        }
        catch (IOException e) {
            System.out.print(e.getMessage());
        }

    }
}
